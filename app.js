import express from "express";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
import dbConfig from "./app/config/db/db";
import routers from "./app/routers";
import socketIO from "./app/routers/socket.io";

var app = express();

//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;
mongoose.connect(dbConfig.db_url);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(routers);
socketIO(app);

const server = app.listen(8080, function() {
  console.log("Listening on port " + server.address().port);
});
