export const conversation_message = {
  ID_USER_SEND_MANDATORY: "User send message is required",
  ID_USER_RECEIVES_MANDATORY: "User receive message is required",
  MESSAGE_MANDATORY: "Message is required"
};
