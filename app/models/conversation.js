import mongoose from "mongoose";
import _ from "lodash";
const ConversationSchema = new mongoose.Schema({
  groupchat: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, "Can't be blank"]
  },
  messages: {
    type: [
      {
        _id: mongoose.Schema.Types.ObjectId,
        name: String,
        time: Number,
        message: String
      }
    ],
    maxlength: 50,
    default: []
  }
});

ConversationSchema.methods.addMessage = function(_id, time, message) {
  ConversationSchema.message = _.concat(this.messages, {
    _id: _id,
    time: time,
    message: message
  });
};
module.exports = mongoose.model("Conversation", ConversationSchema);
