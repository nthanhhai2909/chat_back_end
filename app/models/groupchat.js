import mongoose from "mongoose";
import _ from "lodash";

const GroupChatSchema = new mongoose.Schema({
  users: {
    type: [mongoose.Schema.Types.ObjectId],
    required: [true, "Can't be blank"],
    index: true
  },
  time: {
    type: Number,
    default: _.now()
  }
});

GroupChatSchema.methods.addUser = function(_id) {
  ConversationSchema.users = _.concat(this.users, _id);
};

module.exports = mongoose.model("GroupChat", GroupChatSchema);
