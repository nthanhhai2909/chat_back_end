import mongoose from "mongoose";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import _ from "lodash";
const saltRound = 10;
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Can't be blank"]
  },
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, "Can't be blank"],
    index: true
  },
  password: {
    type: String,
    required: [true, "Can't be blank"]
  },
  sex: { type: String, default: "", enum: ["male", "male"], lowercase: true },
  birthday: { type: Number },
  image: { type: String, default: "" },
  friends: {
    type: [
      {
        id_user: { type: String, unique: true },
        status: {
          type: String,
          enum: ["ACCEPT", "NOT ACCEPT", "WAITING"],
          default: "WAITING"
        }
      }
    ],
    default: []
  }
});

UserSchema.methods.setPassword = function(password) {
  this.password = bcrypt.hashSync(password, saltRound);
};

UserSchema.methods.validatePassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.generateJWT = function() {
  return jwt.sign(
    {
      email: this.email,
      id: this._id
    },
    "secret"
  );
};

UserSchema.methods.addFriend = function(idfri) {
  if (_.map(this.friends, "id_user").indexOf(idfri) == -1) {
    this.friends = _.concat(this.friends, {
      id_user: idfri
    });
    return true;
  }
  return false;
};

UserSchema.methods.updateStatusFriend = function(status, id_user) {
  let friend = _.find(this.friends, { id_user: id_user });
  friend.status = status;
};
UserSchema.methods.toAuthJson = function() {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT()
  };
};
module.exports = mongoose.model("Users", UserSchema);
