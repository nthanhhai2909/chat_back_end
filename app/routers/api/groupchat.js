"use strict";
import express from "express";
import mongoose from "mongoose";
import _ from "lodash";
import jwt from "jsonwebtoken";
import "babel-polyfill";
import auth from "../auth";
import GroupChatModel from "../../models/groupchat";
import ConversationModel from "./conversation";
import * as gp_message from "../../config/message/gp_message";

var router = express.Router();

router.put("/create", auth.authentication, async (req, res, next) => {
  if (!req.body.users) {
    return res.status(200).end();
  }
  // check group has created
  let gpFind;
  try {
    gpFind = await GroupChatModel.findOne({
      users: _.concat(
        req.body.users,
        jwt.verify(req.headers.authorization, "secret").id
      )
    });
  } catch (err) {
    return res.status(404).json(err);
  }
  if (gpFind != null) {
    return res.status(202).end();
  }
  new GroupChatModel({
    users: _.concat(
      req.body.users,
      jwt.verify(req.headers.authorization, "secret").id
    )
  }).save((err, docs) => {
    if (err) {
      return res.status(400).json(err);
    }
    res.status(202).end();
  });
});

router.get("/getlist", auth.authentication, (req, res, next) => {
  const id = jwt.verify(req.headers.authorization, "secret").id;
  GroupChatModel.find({ users: id }, (err, docs) => {
    if (err) {
      return res.status(404).json(err);
    }
    res.status(200).json(docs);
  });
});

router.delete("/delete", auth.authentication, (req, res, next) => {
  if (!req.body.users) {
    return res.status(200).end();
  }
  GroupChatModel.findOneAndDelete({ users: req.body.users }, (err, docs) => {
    if (err) {
      return res.status(404).json(err);
    }
    res.status(200).end();
  });
});

module.exports = router;
