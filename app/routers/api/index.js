import express from "express";
import user from "./user";
import conversation from "./conversation";
import groupchat from "./groupchat";
import auth from "../auth";
var router = express.Router();
router.use("/user", user);
router.use("/conversation", conversation);
router.use("/gpchat", groupchat);
router.get("/auth",  auth.authentication, (req, res) => {
    res.status(200).json({message: "token valid"});
});

module.exports = router;
