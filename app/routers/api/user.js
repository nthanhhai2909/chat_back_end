"use strict";
import express from "express";
import mongoose from "mongoose";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import _ from "lodash";
import "babel-polyfill";

import { user_message } from "../../config/message/user_message";
import validation from "../../utils/validation";
import auth from "../auth";

var router = express.Router();
import UserModel from "../../models/user";

router.post("/signup", async (req, res, next) => {
  const user = req.body;
  // validate mandatory form data

  if (!user.name) {
    return res.status(422).json({
      message: user_message.NAME_MANDATORY
    });
  }

  if (!user.email) {
    return res.status(422).json({
      message: user_message.EMAIL_MANDATORY
    });
  }

  if (!user.password) {
    return res.status(422).json({
      message: user_message.PASSWPRD_MANDATORY
    });
  }

  // validate invalid data
  if (!validation.validationEmail(user.email)) {
    return res.status(422).json({
      message: user_message.EMAIL_INVALID
    });
  }

  if (!validation.validationPassword(user.password)) {
    return res.status(422).json({
      message: user_message.PASSWORD_INVALID
    });
  }
  let userFind;
  try {
    userFind = await UserModel.findOne({ email: user.email });
  } catch (err) {
    return res.status(500).json({
      errors: err
    });
  }

  if (userFind != null) {
    return res.status(409).json({
      message: user_message.EMAIL_ALREADY
    });
  }
  let finalUser = new UserModel(user);
  finalUser.setPassword(user.password);
  finalUser.save((err, docs) => {
    if (err) {
      return res.status(400).json(err);
    }
    res.status(202).json({
      message: user_message.ACCOUNT_HAS_BEEN_CREATED
    });
  });
});

router.post("/login", async (req, res, next) => {
  const user = req.body;

  // validate mandatory form data
  if (!user.email) {
    return res.status(422).json({
      message: user_message.EMAIL_MANDATORY
    });
  }

  if (!user.password) {
    return res.status(422).json({
      message: user_message.PASSWPRD_MANDATORY
    });
  }

  let userFind;
  try {
    userFind = await UserModel.findOne({ email: user.email });
  } catch (err) {
    return res.status(500).json({
      message: "Server not found"
    });
  }
  if (userFind == null) {
    return res.status(404).json({
      message: user_message.EMAIL_OR_PASSWORD_INVALID
    });
  }

  if (userFind.validatePassword(user.password)) {
    return res.status(200).json(userFind.toAuthJson());
  } else {
    return res.status(403).json({
      message: user_message.EMAIL_OR_PASSWORD_INVALID
    });
  }
});

router.get("/addfri", auth.authentication, async (req, res, next) => {
  console.log("adad");
  if (!req.query.fri) {
    return res.status(200).end();
  }

  const _id = jwt.verify(req.headers.authorization, "secret").id;
  let userFind;
  try {
    userFind = await UserModel.findById(_id);
  } catch (err) {
    return res.status(404).json(err);
  }

  // check id user request valid
  try {
    await UserModel.findById(req.query.fri);
  } catch (err) {
    return res.status(404).json(err);
  }

  if (!userFind.addFriend(req.query.fri)) {
    return res.status(202).end();
  }

  userFind.save((err, docs) => {
    if (err) {
      return res.status(500).json(err);
    }
    res.status(202).end();
  });
});

router.get("/resaddfri", auth.authentication, async (req, res, next) => {
  if (!req.query.id) {
    res.status(200).end();
  }
  const _id = jwt.verify(req.headers.authorization, "secret").id;

  let userFind;
  try {
    userFind = await UserModel.findById(_id);
  } catch (err) {
    return res.status(404).json(err);
  }

  userFind.updateStatusFriend("ACCEPT", req.query.id);
  userFind.save((err, docs) => {
    if (err) {
      return res.status(500).json(err);
    }
    res.status(202).end();
  });
});

router.get("/getlistfri", auth.authentication, async (req, res) => {
  const _id = jwt.verify(req.headers.authorization, "secret").id;

  let userFind;
  try {
    userFind = await UserModel.findById(_id);
  } catch (err) {
    return res.status(404).json(err);
  }

  res.status(200).json(
    _.filter(userFind.friends, function(o) {
      return o.status == "ACCEPT";
    })
  );
});

module.exports = router;
