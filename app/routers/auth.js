import jwt from "jsonwebtoken";
import _ from "lodash";
import UserModel from "../models/user";
exports.authentication = (req, res, next) => {
  let result;
  try {
    result = jwt.verify(req.headers.authorization, "secret");
  } catch (err) {
    return res.status(403).json({
        message: "Token invalid"
      
    });
  }
  UserModel.findById(result.id, (err, docs) => {
    if (err) {
      return res.status(403).json({message: err});
    }
    next();
  });
};