import express from "express";
import api from "./api";

var router = express.Router();
router.use("/api", api);

module.exports = router;
