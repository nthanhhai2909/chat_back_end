import http from "http";
import socketIO from "socket.io";
import jwt from "jsonwebtoken";
module.exports = app => {
  var server = http.Server(app);
  var io = socketIO(server);
  const chatIO =  io.of("/chat").on("connection", socket => {
    console.log("new connection", socket.id);
  });

  chatIO.use((socket, next) => {
    if (socket.handshake.query && socket.handshake.query.token) {
      try {
        result = jwt.verify(req.headers.authorization, "secret");
      } catch (err) {
        return next(new Error("Authentication error"));
      }
      next();
    } else {
        return next(new Error("Authentication error"));
    }
  });

  
};
